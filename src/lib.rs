pub trait Discord {
    fn reply(&self, msg: String);
}

pub fn process_vote(discord: &Discord, tokens: &[&str]) -> Option<()> {
    if let Some(&"start") = tokens.get(0) {
        // TODO(jeff): This is a stub response for now.
        // In the future we'll need to parse out the
        // rest of the tokens and branch accordingly
        discord.reply("Starting vote!".to_string());
        return Some(());
    }

    return None;
}

pub fn process_result(discord: &Discord, result: Option<()>) {
    if let None = result {
        discord.reply("That isn't a proper command".to_string());
    }
}

pub fn process_command(discord: &Discord, tokens: &[&str]) -> Option<()> {
    if let Some(command) = tokens.get(0) {
        if command == &"vote" {
            let vote_tokens = &tokens[1..];
            return process_vote(discord, vote_tokens);
        }
    }
    return None;
}

#[cfg(test)]
mod tests {
    use super::*;
}
