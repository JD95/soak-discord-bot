use serenity::client::Client;
use serenity::framework::standard::{
    macros::{command, group},
    CommandResult, StandardFramework,
};
use serenity::model::{channel::Message, gateway::Ready};
use serenity::prelude::{Context, EventHandler};

use std::env;
use std::fmt::Display;

extern crate soak_bot;
use self::soak_bot::{process_command, process_result, Discord};

// A Command is what the bot receives whenever
// someone on discord interacts with it
struct Command<'a> {
    // The interface with the discord message
    // that was given to the bot
    msg: &'a Message,
    ctx: &'a Context,
}

// These are some "methods" that you can use with
// a Command. Mainly we just want the `new` constructor
impl<'a> Command<'a> {
    pub fn new(msg: &'a Message, ctx: &'a Context) -> Command<'a> {
        Command { msg, ctx }
    }
}

// This is the interface to all of the bot
// logic. We use this to hook up the discord
// library to our own
impl<'a> Discord for Command<'a> {
    fn reply(&self, msg: String) {
        self.msg.reply(self.ctx, msg);
    }
}

// An empty type so we can implement the
// EventHandler interface
struct Handler;

// This hooks up the input from discord into
// our bot's logic
impl EventHandler for Handler {
    // Every time someone sends a message on the server
    // this function will be called
    fn message(&self, ctx: Context, msg: Message) {
        // First, split the text by white space
        // ":soak vote start" -> [":soak", "vote", "start"]
        let items: Vec<&str> = msg.content.split_ascii_whitespace().collect();
        // Check to see if they were talking to the bot
        if let Some(&":soak") = items.get(0) {
            // They were talking to the bot, so
            // parse out the command
            let command_tokens = &items[1..];
            // Package everything we got from discord
            // so our bot library can use it
            let discord = Command::new(&msg, &ctx);
            // The bot does it's thing
            let result = process_command(&discord, command_tokens);
            // Tell the user when they've made
            // an error, otherwise give results
            // from whatever command they ran
            process_result(&discord, result);
        }
    }

    fn ready(&self, _: Context, _: Ready) {
        // pass
    }
}

fn main() {
    // Login with a bot token from the environment
    let mut client = Client::new(&env::var("DISCORD_TOKEN").expect("token"), Handler)
        .expect("Error creating client");
    client.with_framework(
        StandardFramework::new()
            // Set prefix used to invoke the bot
            .configure(|c| {
                c.prefix(":soak")
                    // Allow for spaces between commands and groups
                    // eg. :soak message group
                    .with_whitespace((true, true, true))
            }),
    );

    // start listening for events by starting a single shard
    if let Err(why) = client.start() {
        println!("An error occurred while running the client: {:?}", why);
    }
}
