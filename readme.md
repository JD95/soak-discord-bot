# Soak Bot

A bot to help with the School of all Knowledge discord server

# Usage

In Powershell
```bash
$env:DISCORD_TOKEN = "<copy token here>"; cargo run
```

In Bash
```bash
DISCORD_TOKEN="<copy token here>" cargo run
```
